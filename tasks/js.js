const gulp = require('gulp');
const uglify = require('gulp-uglify-es').default;
const rename = require("gulp-rename");
const concat = require('gulp-concat');
const sync = require('browser-sync').get('sync');

const config = require('../config');


gulp.task('js', function() {
    gulp.src(config.src.dev_js + '**/*.js')
        .pipe(uglify())
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest(config.src.js))
        .pipe(sync.stream());
});

gulp.task('concat_js', function() {
    return gulp.src(config.src.dev_js + '**/*.js')
        .pipe(concat('theme-js.js'))
        .pipe(uglify())
        .pipe(gulp.dest(config.src.js));
});

gulp.task('js:watch', function() {
    gulp.watch(config.src.dev_js + '**/*.js', ['js']);
});