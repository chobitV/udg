// =============================================================================
// MOBILE MENU
// =============================================================================

var navBtn = document.getElementById('navBtn'),
    navMenu = document.getElementById('navMenu');

var navOpen = 'menu-open',
    navClose = 'menu-close';

// nav btn - click
navBtn.addEventListener('click', function() {
    if ( this.classList.contains(navClose) ) {
        globalNavOpen();
    } else {
        globalNavClose();
    }
});

// open all nav elements
function globalNavOpen() {
    openNav(navBtn);
    openNav(navMenu);
    scrollLock();
    searchHidden();
}

// close all nav elements
function globalNavClose() {
    closeNav(navBtn);
    closeNav(navMenu);
    scrollUnLock();
}

// open nav el
function openNav(el) {
    el.setAttribute('style', 'will-change: transform;');

    setTimeout(function() {
        el.classList.remove(navClose);

        if (!el.classList.contains(navClose)) {
            el.classList.add(navOpen);
        }
    }, 100);
}

// close nav el
function closeNav(el) {
    el.classList.add(navClose);

    setTimeout(function() {
        el.removeAttribute('style');
    }, 100);
}

// clear nav el
function clearNav() {
    navBtn.classList.remove(navOpen);
    navMenu.classList.remove(navOpen);
    globalNavClose();
}

// check if mobile nav open
function checkIfNavOpen() {
    if (document.getElementById('navMenu').classList.contains(navOpen)) {
        return true;
    }
}

// end NAV





// =============================================================================
// SEARCH
// =============================================================================

const search = document.getElementById('search');
const searchBtn = document.getElementById('searchBtn');
const searchClose = document.getElementById('searchClose');
const searchRootEl = document.querySelector('html');

let classSearchHidden = 'search-hidden';
let classSearchVisible = 'search-visible';

// Search btn
searchBtn.addEventListener('click', function() {
    if (search.classList.contains(classSearchHidden)) {
        searchVisible();
    }
    // else {
    //     search.classList.add(classClose);
    // }
});

// Search close btn
searchClose.addEventListener('click', function() {
    searchHidden();
});

// Search visible
function searchVisible() {
    search.classList.remove(classSearchHidden);
    searchBtn.classList.remove(classSearchHidden);
    searchRootEl.classList.add(classSearchVisible);
    globalNavClose();
}

// Search hidden
function searchHidden() {
    search.classList.add(classSearchHidden);
    searchBtn.classList.add(classSearchHidden);
    searchRootEl.classList.remove(classSearchVisible);
}

// end SEARCH





// =============================================================================
// FORM GROUP
// =============================================================================

(function () {
    const frmGroup = document.querySelectorAll('.frm-group');
    let classFilled = 'frm-group--filled';

    frmGroup.forEach((item) => {
        const getInput = item.querySelector('.frm-group__input');
        const getLabel = item.querySelector('.frm-group__label');

        getInput.addEventListener('focus', e => {
            e.preventDefault();
            item.classList.add(classFilled);
        });

        getInput.addEventListener('blur', e => {
            if (getInput.value === '') {
                item.classList.remove(classFilled);
            }
        });
    });
})();

// end FORM GROUP





// =============================================================================
// OPTIMIZED RESIZE
// =============================================================================

var optimizedResize = (function() {

    var callbacks = [],
        running = false;

    // fired on resize event
    function resize() {

        if (!running) {
            running = true;

            if (window.requestAnimationFrame) {
                window.requestAnimationFrame(runCallbacks);
            } else {
                setTimeout(runCallbacks, 66);
            }
        }

    }

    // run the actual callbacks
    function runCallbacks() {

        callbacks.forEach(function(callback) {
            callback();
        });

        running = false;
    }

    // adds callback to loop
    function addCallback(callback) {

        if (callback) {
            callbacks.push(callback);
        }

    }

    return {
        // public method to add additional callback
        add: function(callback) {
            if (!callbacks.length) {
                window.addEventListener('resize', resize);
            }
            addCallback(callback);
        }
    }
}());

// start process
optimizedResize.add(function() {
    if (getWindowWidth() >= 750 ) {
        clearNav();
    }
});

// end OPTIMIZED RESIZE




// =============================================================================
// OPTIMIZED SCROLL
// =============================================================================

var last_known_scroll_position = 0;
var ticking = false;

function doSomething(scroll_pos) {

}

window.addEventListener('scroll', function(e) {
    last_known_scroll_position = window.scrollY;
    if (!ticking) {
        window.requestAnimationFrame(function() {
            doSomething(last_known_scroll_position);
            ticking = false;
        });
    }
    ticking = true;
});

// end OPTIMIZED SCROLL





// =============================================================================
// GET WINDOW WIDTH
// =============================================================================

function getWindowWidth() {
    var w = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
    return w;
};

// end GET WINDOW WIDTH






// =============================================================================
// ENABLE/DISABLE SCROLL
// =============================================================================

// disable scroll
function scrollLock() {
    document.querySelector('html').classList.add('scroll-lock');
}

// enable scroll
function scrollUnLock() {
    document.querySelector('html').classList.remove('scroll-lock');
}

// end ENEBLE/DISABLE SCROLL